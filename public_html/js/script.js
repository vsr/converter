/* Author: 

*/
var cvtr = { hash:'' };
$(function(cvtr){

    $(".unit-container").height( $("#quantity-container").height() );

    var $a = $("#quantity-container li a"),
        $inputs = $(":input", "#unit-container");
    cvtr.quantity_click = function() {
        $a.removeClass('active');
        $(this).addClass('active');
        var hash = $(this).attr('href');
        $inputs.attr('disabled', true);
        $(":input", hash).attr('disabled', false).val('');
        $(".unit-container").css('left', - $(hash).position().left )
        $('.unit-container').height( $(hash).height() + $('.header').height() + 90  )
    }
    cvtr.calculate = function(ev) {
        if(ev.which===9 || ev.which ===16 || ev.ctrlKey || ev.shiftKey || ev.altKey) {
            return false;
        }

        var me = $(this),
            quantity = me.parents(".unit-list").attr('id'),
            me_unit = me.attr('name'),
            me_unit_value = me.attr('data-x'),
            x = me.val();
        x = Number(x);
        window.location.hash = '#' + quantity + '/!' + me_unit + '/!' + me.val();
        if ( isNaN(x) ) {    me.addClass('error');    }
        else {    me.removeClass('error');    }
        var y = Number(eval(me_unit_value)),
            to_fields = $('input:not([name="'+me_unit+'"])', '#'+quantity);

        for(var i=0,j=to_fields.length;i<j;i++) {
            var formula = $(to_fields[i]).attr('data-y'),
                result = Number( eval(formula) );
            $(to_fields[i]).val( result );
            if ( isNaN(result) ) {    $(to_fields[i]).addClass('error');    }
            else {    $(to_fields[i]).removeClass('error');    }
        }
    }
    $a.click( cvtr.quantity_click );
    $(".unit-list :input").keyup( cvtr.calculate );
    
    cvtr.hash_change = function() {
        if( window.location.hash ) {
            var hash_parts = decodeURI(window.location.hash) .split('/!') ;
            if( hash_parts[0] ) {    $( "#quantity-container li a[href="+ hash_parts[0] + "]" ).click() }
            if( hash_parts[1] ) {
                var inp = $(":input[name='"+hash_parts[1]+"']", hash_parts[0]);
                inp.focus();
                if( hash_parts[2] ) {
                    inp.val( hash_parts[2] ).keyup();
                }
            }
        }
    }
    
    cvtr.hash_change();
    'onhashchange' in window && (window.onhashchange = cvtr.hash_change);
});




